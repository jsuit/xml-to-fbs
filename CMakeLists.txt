cmake_minimum_required(VERSION 3.11)

project(xml-to-fbs LANGUAGES CXX)

include(FetchContent)

FetchContent_Declare(
  expat
  GIT_REPOSITORY https://github.com/libexpat/libexpat
)

FetchContent_GetProperties(expat)
if(NOT expat_POPULATED)
  FetchContent_Populate(expat)
  add_subdirectory(${expat_SOURCE_DIR}/expat ${expat_BINARY_DIR}
    EXCLUDE_FROM_ALL)
  add_library(libexpat INTERFACE)
  target_include_directories(libexpat
    INTERFACE ${expat_SOURCE_DIR}/expat/lib)
  target_link_libraries(libexpat
    INTERFACE expat)
endif()

FetchContent_Declare(
  catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v2.5.0
)
FetchContent_GetProperties(catch2)
if(NOT catch2_POPULATED)
  FetchContent_Populate(catch2)
  add_subdirectory(${catch2_SOURCE_DIR} ${catch2_BINARY_DIR}
    EXCLUDE_FROM_ALL)
endif()

FetchContent_Declare(
  flatbuffers
  GIT_REPOSITORY  https://github.com/google/flatbuffers
)
FetchContent_GetProperties(flatbuffers)
if(NOT flatbuffers_POPULATED)
  FetchContent_Populate(flatbuffers)
  add_subdirectory(${flatbuffers_SOURCE_DIR} ${flatbuffers_BINARY_DIR}
    EXCLUDE_FROM_ALL)
endif()

FetchContent_Declare(
  date
  GIT_REPOSITORY https://github.com/HowardHinnant/date.git
)
FetchContent_GetProperties(date)
if(NOT date_POPULATED)
  FetchContent_Populate(date)
  set(ENABLE_DATE_TESTING OFF)
  set(USE_SYSTEM_TZ_DB ON)
  add_subdirectory(${date_SOURCE_DIR} ${date_BINARY_DIR}
    EXCLUDE_FROM_ALL)
endif()

if(CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
    add_definitions(/W2)
else()
    add_definitions(-Wall)
endif()


add_subdirectory(xml_fbs)
add_subdirectory(examples)