#include <fstream>
#include <iostream>
#include "date/date.h"
#include "flatbuffers/minireflect.h"
#include "union_case_generated.h"
#include "xml_to_fbs_parser.h"

namespace flx {

template <>
flatbuffers::Offset<union_case::TDate> CreateFromCData(
    flatbuffers::FlatBufferBuilder &builder, const std::string &cdata) {
  std::istringstream is(cdata);
  date::year_month_day dte;

  is >> date::parse("%F", dte);

  std::cout << dte << std::endl;

  auto year = static_cast<int>(dte.year());
  auto month = static_cast<unsigned>(dte.month());
  auto day = static_cast<unsigned>(dte.day());

  return union_case::CreateTDate(builder, year, month, day);
}

template <>
flatbuffers::Offset<union_case::TString> CreateFromCData(
    flatbuffers::FlatBufferBuilder &builder, const std::string &cdata) {
  return union_case::CreateTString(builder, builder.CreateString(cdata));
}

}  // namespace flx

using FbsTStringHandler = flx::FbsCDataHandler<union_case::TString>;
using FbsTDateHandler = flx::FbsCDataHandler<union_case::TDate>;

class FbsHandlerRoot : public flx::FbsTableHandler<union_case::Root> {
 public:
  FbsHandlerRoot(flatbuffers::FlatBufferBuilder *builder)
      : FbsTableHandler(builder, 0, false) {
    field_map_ = {
        {"date0",
         std::make_shared<FbsTDateHandler>(builder, FbsType::VT_DTE0, false)}
        ,{"date_as_str",
         std::make_shared<
             flx::FbsUnionHandler<FbsTStringHandler, union_case::UDate>>(
             builder,
             std::make_shared<FbsTStringHandler>(builder, FbsType::VT_DTE1,
                                                 false),
             FbsType::VT_DTE1_TYPE, FbsType::VT_DTE1,
             union_case::UDate::TString)}};
  }
};

int main(int argc, char *argv[]) {
  if (argc <= 1) {
    std::cout << "Usage: " << argv[0] << " input.xml" << std::endl;
    exit(-1);
  }
  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cout << "Unable to open " << argv[1] << std::endl;
    exit(-1);
  }

  flx::XmlToFbsParser<FbsHandlerRoot> parser("root");

  std::string line;
  while (std::getline(fin, line)) {
    parser.Parse(line.c_str(), -1, fin.eof());
  }

  auto builder = parser.GetFlatBufferBuilder();

  std::cout << "buffer size = " << builder->GetSize() << std::endl;

  auto s = flatbuffers::FlatBufferToString(builder->GetBufferPointer(),
                                           union_case::RootTypeTable());
  std::cout << s << std::endl;
  return 0;
}
