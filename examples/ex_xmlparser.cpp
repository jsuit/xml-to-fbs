#include <fstream>
#include <iostream>
#include "xml_parser.h"

class MyHandler : public flx::XmlElementHandler {
 public:
  virtual ~MyHandler() {}
  virtual bool Open(flx::XmlParser *parser, const XML_Char *name,
                    const XML_Char **atts) override {
    std::cout << indent() << "<" << name << ">" << std::endl;
    ++depth_;
    return true;
  }

  virtual bool Close(flx::XmlParser *parser, const XML_Char *name,
                     const std::string &data) override {
    std::cout << indent() << "data(" << name << "): " << data << std::endl;
    --depth_;
    return false;
  }

 private:
  std::string indent() { return std::string(depth_ * 4, ' '); }
  int depth_;
};

int main(int argc, char *argv[]) {
  if (argc <= 1) {
    std::cout << "Usage: " << argv[0] << " input.xml" << std::endl;
    exit(-1);
  }
  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cout << "Unable to open " << argv[1] << std::endl;
    exit(-1);
  }

  flx::XmlParser parser;
  MyHandler my_handler;
  parser.PushElementHandler(&my_handler);

  std::string line;

  while (std::getline(fin, line)) {
    parser.Parse(line.c_str(), -1, fin.eof());
  }
  return 0;
}