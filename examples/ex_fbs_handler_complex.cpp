#include <fstream>
#include <iostream>
#include "complex_case_generated.h"
#include "date/date.h"
#include "flatbuffers/minireflect.h"
#include "xml_to_fbs_parser.h"

namespace flx {

template <>
ns::Gender CreateFromCData(flatbuffers::FlatBufferBuilder &builder,
                           const std::string &cdata) {
  return flx::ParseEnum<ns::Gender>(cdata, ns::EnumNamesGender(),
                                    ns::EnumValuesGender());
}

template <>
ns::Kind CreateFromCData(flatbuffers::FlatBufferBuilder &builder,
                         const std::string &cdata) {
  return flx::ParseEnum<ns::Kind>(cdata, ns::EnumNamesKind(),
                                  ns::EnumValuesKind());
}

template <>
ns::Date CreateFromCData(flatbuffers::FlatBufferBuilder &builder,
                         const std::string &cdata) {
  std::istringstream is(cdata);
  date::year_month_day dte;

  is >> date::parse("%F", dte);

  std::cout << dte << std::endl;

  auto year = static_cast<int>(dte.year());
  auto month = static_cast<unsigned>(dte.month());
  auto day = static_cast<unsigned>(dte.day());
  auto ret = ns::Date(year, month, day);

  return ret;
}

template <>
flatbuffers::Offset<ns::TDate> CreateFromCData(
    flatbuffers::FlatBufferBuilder &builder, const std::string &cdata) {
  std::istringstream is(cdata);
  date::year_month_day dte;

  is >> date::parse("%F", dte);

  std::cout << dte << std::endl;

  auto year = static_cast<int>(dte.year());
  auto month = static_cast<unsigned>(dte.month());
  auto day = static_cast<unsigned>(dte.day());

  return ns::CreateTDate(builder, year, month, day);
}
}  // namespace flx

using FbsStringHandler = flx::FbsCDataHandler<flatbuffers::String>;
using FbsUInt32Handler = flx::FbsCDataHandler<uint32_t>;
using FbsFloatHandler = flx::FbsCDataHandler<float>;

using FbsGenderHandler = flx::FbsCDataHandler<ns::Gender>;
using FbsKindHandler = flx::FbsCDataHandler<ns::Kind>;
using FbsDateHandler = flx::FbsCDataHandler<ns::Date>;
using FbsTDateHandler = flx::FbsCDataHandler<ns::TDate>;

class FbsListOHandler : public flx::FbsDeepHandler {
 public:
  FbsListOHandler(flatbuffers::FlatBufferBuilder *builder)
      : flx::FbsDeepHandler(builder) {
    field_map_ = {{"elem", std::make_shared<FbsStringHandler>(
                               builder, ns::Other::VT_LIST_O, true)}};
  }
};

class FbsHandlerOther : public flx::FbsTableHandler<ns::Other> {
 public:
  FbsHandlerOther(flatbuffers::FlatBufferBuilder *builder)
      : FbsTableHandler(builder, ns::Order::VT_OTHER_DATA, false) {
    field_map_ = {{"count", std::make_shared<FbsUInt32Handler>(
                                builder, FbsType::VT_COUNT, false)},
                  {"list_O", std::make_shared<FbsListOHandler>(builder)},
                  {"kind", std::make_shared<FbsKindHandler>(
                               builder, FbsType::VT_KIND, false)}};
  }
};

class FbsListOfItemsHandler : public flx::FbsDeepHandler {
 public:
  FbsListOfItemsHandler(flatbuffers::FlatBufferBuilder *builder,
                        flatbuffers::voffset_t field)
      : flx::FbsDeepHandler(builder) {
    field_map_ = {
        {"item", std::make_shared<FbsStringHandler>(builder, field, true)}};
  }
};

/*
class FbsHandlerAChoice : public flx::FbsChoiceHandler {
public:
  FbsHandlerAChoice(flatbuffers::FlatBufferBuilder *builder, ) {

  }
};
*/

class FbsHandlerOrder : public flx::FbsTableHandler<ns::Order> {
 public:
  FbsHandlerOrder(flatbuffers::FlatBufferBuilder *builder)
      : FbsTableHandler(builder, 0, false) {
    field_map_ = {
        {"name",
         std::make_shared<FbsStringHandler>(builder, FbsType::VT_NAME, false)},
        {"Gender", std::make_shared<FbsGenderHandler>(
                       builder, FbsType::VT_GENDER_CODE, false)},
        {"DateOfSale", std::make_shared<FbsDateHandler>(
                           builder, FbsType::VT_DATE_SALE, false)},
        {"Date", std::make_shared<FbsDateHandler>(
                     builder, FbsType::VT_LIST_OF_DATES, true)},
        {"price",
         std::make_shared<FbsFloatHandler>(builder, FbsType::VT_PRICE, false)},
        {"other", std::make_shared<FbsHandlerOther>(builder)},
        {"list_A",
         std::make_shared<FbsListOfItemsHandler>(builder, FbsType::VT_LIST_A)},
        {"list_B",
         std::make_shared<FbsListOfItemsHandler>(builder, FbsType::VT_LIST_B)},
        {"Choice_TDate",
         std::make_shared<flx::FbsUnionHandler<FbsTDateHandler, ns::Choice>>(
             builder,
             std::make_shared<FbsTDateHandler>(builder,
                                               FbsType::VT_FIELD_CHOICE, false),
             FbsType::VT_FIELD_CHOICE_TYPE, FbsType::VT_FIELD_CHOICE,
             ns::Choice::TDate)},

        {"Choice_Date",
         std::make_shared<flx::FbsUnionHandler<FbsDateHandler, ns::Choice>>(
             builder,
             std::make_shared<FbsDateHandler>(builder, FbsType::VT_FIELD_CHOICE,
                                              false),
             FbsType::VT_FIELD_CHOICE_TYPE, FbsType::VT_FIELD_CHOICE,
             ns::Choice::Date)},
        {"class", std::make_shared<FbsKindHandler>(
                      builder, FbsType::VT_LIST_OF_CLASSES, true)}};
  }
};

int main(int argc, char *argv[]) {
  if (argc <= 1) {
    std::cout << "Usage: " << argv[0] << " input.xml" << std::endl;
    exit(-1);
  }
  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cout << "Unable to open " << argv[1] << std::endl;
    exit(-1);
  }

  flx::XmlToFbsParser<FbsHandlerOrder> parser("order");

  std::string line;
  while (std::getline(fin, line)) {
    parser.Parse(line.c_str(), -1, fin.eof());
  }

  auto builder = parser.GetFlatBufferBuilder();

  std::cout << "buffer size = " << builder->GetSize() << std::endl;

  auto s = flatbuffers::FlatBufferToString(builder->GetBufferPointer(),
                                           ns::OrderTypeTable());
  std::cout << s << std::endl;
  return 0;
}
