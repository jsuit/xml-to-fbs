#include "greeting_generated.h"
#include "flatbuffers/minireflect.h"
#include "flatbuffers/reflection.h"

#include <vector>
#include <iostream>

using namespace greetings;

int main()
{
  flatbuffers::FlatBufferBuilder builder(1024);

  flatbuffers::uoffset_t offset_greeting = builder.CreateString("Hello").o;

  std::vector<flatbuffers::uoffset_t> offset_names;

  offset_names.push_back(builder.CreateString("moon").o);
  offset_names.push_back(builder.CreateString("sun").o);
  offset_names.push_back(builder.CreateString("earth").o);

  std::vector<flatbuffers::Offset<flatbuffers::String>> names_vector;
  for (auto it = offset_names.begin(); it != offset_names.end(); it++) {
    names_vector.push_back(flatbuffers::Offset<flatbuffers::String>(*it));
  }
  /*
  names_vector.push_back(builder.CreateString("moon"));
  names_vector.push_back(builder.CreateString("sun"));
  names_vector.push_back(builder.CreateString("earth"));
  */
  auto names = builder.CreateVector(names_vector);

  TargetBuilder target_builder(builder);
  target_builder.add_names(names);
  auto target = target_builder.Finish();

  HelloBuilder root_builder(builder);

  flatbuffers::Offset<flatbuffers::String> greeting(offset_greeting);
  root_builder.add_greeting(greeting);

  root_builder.add_target(target);

  auto root = root_builder.Finish();
  builder.Finish(root);

  auto s = flatbuffers::FlatBufferToString(builder.GetBufferPointer(), HelloTypeTable());

  std::cout << "buffer size = " << builder.GetSize() << std::endl;
  
  std::cout << s << std::endl;
  return 0;
}