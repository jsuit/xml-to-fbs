#include <fstream>
#include <iostream>
#include "flatbuffers/minireflect.h"
#include "greeting_generated.h"
#include "xml_to_fbs_parser.h"

//
// <greetings::Target,
//   <"name", flx::FbsSimpleHandler, flatbuffers::String,
//   greetings::Target::VT_NAMES>>
//

using FbsStringHandler = flx::FbsCDataHandler<flatbuffers::String>;

class FbsHandlerTarget : public flx::FbsTableHandler<greetings::Target> {
 public:
  FbsHandlerTarget(flatbuffers::FlatBufferBuilder *builder,
                   flatbuffers::voffset_t vt_field)
      : FbsTableHandler(builder, vt_field) {
    field_map_ = {{"name", std::make_shared<FbsStringHandler>(
                               builder, FbsType::VT_NAMES, true)}};
  }
};

class FbsHandlerHello : public flx::FbsTableHandler<greetings::Hello> {
 public:
  FbsHandlerHello(flatbuffers::FlatBufferBuilder *builder)
      : FbsTableHandler(builder, 0) {
    field_map_ = {{"greeting", std::make_shared<FbsStringHandler>(
                                   builder, FbsType::VT_GREETING)},
                  {"target", std::make_shared<FbsHandlerTarget>(
                                 builder, FbsType::VT_TARGET)}};
  }
};

int main(int argc, char *argv[]) {
  if (argc <= 1) {
    std::cout << "Usage: " << argv[0] << " input.xml" << std::endl;
    exit(-1);
  }
  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cout << "Unable to open " << argv[1] << std::endl;
    exit(-1);
  }

  flx::XmlToFbsParser<FbsHandlerHello> parser("hello");

  std::string line;
  while (std::getline(fin, line)) {
    parser.Parse(line.c_str(), -1, fin.eof());
  }

  auto builder = parser.GetFlatBufferBuilder();

  std::cout << "buffer size = " << builder->GetSize() << std::endl;

  auto s = flatbuffers::FlatBufferToString(builder->GetBufferPointer(),
                                           greetings::HelloTypeTable());

  std::cout << s << std::endl;
  return 0;
}