#include "complex_case_generated.h"
#include "flatbuffers/minireflect.h"
#include "flatbuffers/reflection.h"

#include <iostream>
#include <vector>

int main() {
  flatbuffers::FlatBufferBuilder builder(1024);

  auto str = ns::Date(2020, 7, 25);
  //auto off_str = builder.CreateStruct(&str);
  auto off_tdate = ns::CreateTDate(builder, 2020, 7, 31);

  ns::OrderBuilder order_builder_(builder);

  order_builder_.add_date_sale(&str);
  //order_builder_.add_field_choice_type(ns::Choice::Date);
  //order_builder_.add_field_choice(off_str.Union());
  order_builder_.add_field_choice(off_tdate.Union());
  order_builder_.add_field_choice_type(ns::Choice::TDate);

  /*
  builder_.add_list_of_classes(list_of_classes);
  builder_.add_list_of_dates(list_of_dates);
  builder_.add_date_sale(date_sale);
  builder_.add_other_data(other_data);
  builder_.add_price(price);
  builder_.add_list_B(list_B);
  builder_.add_list_A(list_A);
  builder_.add_name(name);
  builder_.add_kind(kind);
  builder_.add_gender_code(gender_code);
  */

  auto root = order_builder_.Finish();
  builder.Finish(root);

  auto s = flatbuffers::FlatBufferToString(builder.GetBufferPointer(),
                                           ns::OrderTypeTable());

  std::cout << "buffer size = " << builder.GetSize() << std::endl;

  std::cout << s << std::endl;
  return 0;
}
