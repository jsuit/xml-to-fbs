#include "union_case_generated.h"
#include "flatbuffers/minireflect.h"

#include <iostream>
#include <vector>

#define UNION_AS_STRING

int main() {
  flatbuffers::FlatBufferBuilder builder(1024);

  auto tdate0 = union_case::CreateTDate(builder, 2017, 01, 31);
#if defined(UNION_AS_STRING)
  auto tdate1 =
      union_case::CreateTString(builder, builder.CreateString("2018-07-31"));
#else
  auto tdate1 = union_case::CreateTDate(builder, 2018, 07, 31);
#endif
  union_case::RootBuilder root_builder(builder);

  root_builder.add_dte0(tdate0);
#if defined(UNION_AS_STRING)
  root_builder.add_dte1_type(union_case::UDate::TString);
#else
  root_builder.add_dte1_type(union_case::UDate::TDate);
#endif
  root_builder.add_dte1(tdate1.Union());

  auto root = root_builder.Finish();
  builder.Finish(root);

  auto s = flatbuffers::FlatBufferToString(builder.GetBufferPointer(),
                                           union_case::RootTypeTable());

  std::cout << "buffer size = " << builder.GetSize() << std::endl;

  std::cout << s << std::endl;
  return 0;
}
