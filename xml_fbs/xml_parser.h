#ifndef XML_PARSER_H_
#define XML_PARSER_H_

#include <cstring>
#include <memory>
#include <stack>
#include <string>
#include "expat.h"

namespace flx {

class XmlParser;

class XmlElementHandler {
 public:
  XmlElementHandler() {}
  virtual ~XmlElementHandler() {}
  virtual bool Open(XmlParser *parser, const XML_Char *name,
                    const XML_Char **atts) {
    return false;
  }

  virtual bool Close(XmlParser *parser, const XML_Char *name,
                     const std::string &data) {
    return false;
  }

  virtual bool Enter(XmlParser *parser, const XML_Char *name,
                     const XML_Char **atts) {
    return false;
  }

  virtual void Activate(XmlParser *parser, const XML_Char *name) {}
};

class XmlParser {
 public:
  XmlParser() {
    parser_ = XML_ParserCreate(NULL);
    XML_SetCharacterDataHandler(parser_, XmlParser::CharacterDataCb);
    XML_SetStartElementHandler(parser_, XmlParser::StartElementCb);
    XML_SetEndElementHandler(parser_, XmlParser::EndElementCb);
    XML_SetUserData(parser_, this);
  }

  ~XmlParser() { XML_ParserFree(parser_); }

  XML_Status Parse(const char *buffer, int len = -1, bool is_final = false) {
    if (len == -1) {
      len = std::strlen(buffer);
    }
    return XML_Parse(parser_, buffer, len, is_final);
  }

  XML_Error GetErrorCode() { return XML_GetErrorCode(parser_); }

  const XML_LChar *GetErrorString(XML_Error code) {
    return XML_ErrorString(code);
  }

  const XML_LChar *GetErrorString() {
    return this->GetErrorString(this->GetErrorCode());
  }

  void PushElementHandler(XmlElementHandler *handler) {
    stack_handler_.push(handler);
  }

  XmlElementHandler *GetCurrentHandler() {
    return stack_handler_.size() > 0 ? stack_handler_.top() : nullptr;
  }

  XmlElementHandler *PopElementHandler() {
    XmlElementHandler *top = GetCurrentHandler();

    stack_handler_.pop();
    return top;
  }

  bool GetCollectData() { return collect_cdata_; }

  void SetCollectData(bool collect) {
    collect_cdata_ = collect;
    cdata_.clear();
  }

  void CollectDataOn() {
    collect_cdata_ = true;
    cdata_.clear();
  }

  void CollectDataOff() {
    collect_cdata_ = false;
    cdata_.clear();
  }

 private:
  XML_Parser parser_;
  std::stack<XmlElementHandler *> stack_handler_;
  bool collect_cdata_;
  std::string cdata_;

  static void StartElementCb(void *user_data, const XML_Char *name,
                             const XML_Char **atts);
  static void EndElementCb(void *user_data, const XML_Char *name);
  static void CharacterDataCb(void *user_data, const XML_Char *buffer, int len);
};

}  // namespace flx

#endif
