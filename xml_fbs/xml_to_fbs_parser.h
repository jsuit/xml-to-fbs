#ifndef FBS_HANDLER_H_
#define FBS_HANDLER_H_

#include <iostream>
#include <type_traits>
#include <unordered_map>
#include "flatbuffers/flatbuffers.h"
#include "xml_parser.h"

namespace flx {

#define ENABLE_TRACE 1
#if ENABLE_TRACE
#define trace(x)                                                      \
  {                                                                   \
    std::cout << "[" << __FILE__ << "][" << __FUNCTION__ << "][Line " \
              << __LINE__ << "] " << x << std::endl;                  \
  }
#else
#define trace(...)
#endif

class FbsHandler : public XmlElementHandler {
 public:
  FbsHandler(flatbuffers::FlatBufferBuilder *fbs_builder,
             flatbuffers::voffset_t vt_field, bool collect_cdata = false)
      : vt_field_(vt_field),
        fbs_builder_(fbs_builder),
        collect_cdata_(collect_cdata) {}

  virtual ~FbsHandler() {}

  virtual flatbuffers::uoffset_t GetOffset() { return 0; }

  virtual void Build() = 0;
  virtual void Compose() = 0;

  virtual bool Enter(XmlParser *parser, const XML_Char *name,
                     const XML_Char **atts) override {
    match_name_.assign(name);
    return collect_cdata_;
  }

  bool GetCollectCData() { return collect_cdata_; }
  flatbuffers::voffset_t GetVtField() { return vt_field_; }

 protected:
  flatbuffers::voffset_t vt_field_;
  flatbuffers::FlatBufferBuilder *fbs_builder_;
  bool collect_cdata_;
  std::string match_name_;
};

template <typename T, class S = void>
struct enum_rep_t {
  typedef void type;
};

template <typename T>
struct enum_rep_t<T, typename std::enable_if<std::is_enum<T>::value>::type> {
  typedef std::underlying_type_t<T> type;
};

template <typename T>
class FbsMemberHandler : public FbsHandler {
 public:
  using FbsType = T;
  using ItemType = typename std::conditional<
      std::is_enum<T>::value, typename enum_rep_t<T>::type,
      typename std::conditional<std::is_base_of<flatbuffers::Table, T>::value ||
                                    std::is_same<flatbuffers::String, T>::value,
                                flatbuffers::Offset<T>, T>::type>::type;

  FbsMemberHandler(flatbuffers::FlatBufferBuilder *fbs_builder,
                   flatbuffers::voffset_t vt_field, bool is_array = false,
                   bool collect_cdata = false)
      : FbsHandler(fbs_builder, vt_field, collect_cdata),
        is_array_(is_array),
        array_offset_(0) {}

  virtual void Build() override {
    if (is_array_) {
      array_offset_ = CreateVector();
    }
  }

  template <typename U = T,
            typename std::enable_if<std::is_scalar<U>::value, int>::type = 0>
  void AddSingleItem() {
    fbs_builder_->AddElement(vt_field_, elements_[0], ItemType());
  }

  template <typename U = T, typename std::enable_if<
                                std::is_base_of<flatbuffers::Table, U>::value ||
                                    std::is_same<flatbuffers::String, U>::value,
                                int>::type = 0>
  void AddSingleItem() {
    fbs_builder_->AddOffset(vt_field_, elements_[0]);
  }

  template <typename U = T,
            typename std::enable_if<
                !std::is_scalar<U>::value &&
                    !std::is_base_of<flatbuffers::Table, U>::value &&
                    !std::is_same<flatbuffers::String, U>::value,
                int>::type = 0>
  void AddSingleItem() {
    fbs_builder_->AddStruct(vt_field_, &elements_[0]);
  }

  virtual void Compose() override {
    if (is_array_) {
      fbs_builder_->AddElement(vt_field_, fbs_builder_->ReferTo(array_offset_),
                               static_cast<flatbuffers::uoffset_t>(0));
    } else if (elements_.size()) {
      AddSingleItem();
    }
  }

  virtual bool Close(XmlParser *parser, const XML_Char *name,
                     const std::string &cdata) override {
    bool pop_on_close = false;
    if (!match_name_.compare(name)) {
      trace("matched end of element " << name);
      // invoke VisitMemberOffsets to ensure each member is created prior to
      // invoke StartTable
      auto item = BuildSelf(cdata);
      elements_.push_back(item);
      pop_on_close = true;
    } else {
      trace("reached end of unprocessed element " << name);
    }
    return pop_on_close;
  }

  const std::vector<ItemType> &GetElements() { return elements_; }

 protected:
  template <class X, class Y>
  friend class FbsUnionHandler;

  virtual ItemType BuildSelf(const std::string &cdata) = 0;

  template <typename U = T,
            typename std::enable_if<std::is_scalar<U>::value, int>::type = 0>
  flatbuffers::uoffset_t CreateVector() {
    auto vec = this->fbs_builder_->CreateVector(elements_);
    return vec.o;
  }

  template <typename U = T,
            typename std::enable_if<
                !std::is_scalar<U>::value &&
                    !std::is_base_of<flatbuffers::Table, U>::value &&
                    !std::is_same<flatbuffers::String, U>::value,
                int>::type = 0>
  flatbuffers::uoffset_t CreateVector() {
    auto vec = this->fbs_builder_->CreateVectorOfStructs(elements_);
    return vec.o;
  }

  template <typename U = T, typename std::enable_if<
                                std::is_base_of<flatbuffers::Table, U>::value ||
                                    std::is_same<flatbuffers::String, U>::value,
                                int>::type = 0>
  flatbuffers::uoffset_t CreateVector() {
    auto vec = this->fbs_builder_->CreateVector(elements_);
    return vec.o;
  }

  bool is_array_;
  std::vector<ItemType> elements_;
  flatbuffers::uoffset_t array_offset_;
};

template <typename E>
E ParseEnum(const std::string &cdata, const char *const *names,
            const E *values) {
  E ret = values[0];

  size_t i;
  for (i = 0; names[i]; ++i) {
    if (!cdata.compare(names[i])) {
      break;
    }
  }
  if (names[i]) {
    ret = values[i];
  }
  return ret;
}

template <typename T>
T CreateFromCData(flatbuffers::FlatBufferBuilder &fbs_builder,
                  const std::string &cdata);

template <>
float CreateFromCData(flatbuffers::FlatBufferBuilder &fbs_builder,
                      const std::string &cdata) {
  return std::stof(cdata);
}

template <>
double CreateFromCData(flatbuffers::FlatBufferBuilder &fbs_builder,
                       const std::string &cdata) {
  return std::stod(cdata);
}

template <>
uint32_t CreateFromCData(flatbuffers::FlatBufferBuilder &fbs_builder,
                         const std::string &cdata) {
  return std::stoul(cdata);
}

template <>
flatbuffers::Offset<flatbuffers::String> CreateFromCData(
    flatbuffers::FlatBufferBuilder &fbs_builder, const std::string &cdata) {
  return fbs_builder.CreateString(cdata);
}

template <typename T>
class FbsCDataHandler : public FbsMemberHandler<T> {
 public:
  using ItemType = typename flx::FbsMemberHandler<T>::ItemType;

  FbsCDataHandler(flatbuffers::FlatBufferBuilder *builder,
                  flatbuffers::voffset_t vt_field, bool is_array = false)
      : FbsMemberHandler<T>(builder, vt_field, is_array, true) {}
  ~FbsCDataHandler() {}

 protected:
  template <typename U = T,
            typename std::enable_if<std::is_enum<U>::value, int>::type = 0>
  ItemType BuildContent(const std::string &cdata) {
    return static_cast<ItemType>(
        CreateFromCData<U>(*(this->fbs_builder_), cdata));
  }

  template <typename U = T,
            typename std::enable_if<!std::is_enum<U>::value, int>::type = 0>
  ItemType BuildContent(const std::string &cdata) {
    return CreateFromCData<ItemType>(*(this->fbs_builder_), cdata);
  }

  virtual ItemType BuildSelf(const std::string &cdata) override {
    return this->BuildContent(cdata);
  }
};

struct FbsGroupHelper {
  typedef std::unordered_map<std::string, std::shared_ptr<FbsHandler>>
      FieldConfigMap;

  bool Open(XmlParser *parser, const XML_Char *name, const XML_Char **atts,
            FieldConfigMap &field_map) {
    bool collect_cdata = false;
    auto it = field_map.find(name);

    if (it == field_map.end()) {
      trace("skiping element: <" << name << ">");
    } else {
      collect_cdata = EnterChildHandler(it->second.get(), parser, name, atts);
    }
    return collect_cdata;
  }

  bool EnterChildHandler(FbsHandler *child_handler, XmlParser *parser,
                         const XML_Char *name, const XML_Char **atts) {
    parser->PushElementHandler(child_handler);
    return child_handler->Enter(parser, name, atts);
  }

  void BuildChildren(FieldConfigMap &field_map) {
    for (auto it : field_map) {
      it.second->Build();
    }
  }

  void ComposeChildren(FieldConfigMap &field_map) {
    for (auto it : field_map) {
      it.second->Compose();
    }
  }
};

template <typename T>
class FbsTableHandler : public FbsMemberHandler<T>, public FbsGroupHelper {
 public:
  using ItemType = typename flx::FbsMemberHandler<T>::ItemType;
  using FieldConfigMap = FbsGroupHelper::FieldConfigMap;

  FbsTableHandler(flatbuffers::FlatBufferBuilder *builder,
                  flatbuffers::voffset_t vt_field, bool is_array = false)
      : FbsMemberHandler<T>(builder, vt_field, is_array, false), offset_(0) {}

  ~FbsTableHandler() {}

  virtual bool Open(XmlParser *parser, const XML_Char *name,
                    const XML_Char **atts) override {
    return FbsGroupHelper::Open(parser, name, atts, field_map_);
  }

  flatbuffers::uoffset_t GetOffset() { return offset_; }

 protected:
  virtual ItemType BuildSelf(const std::string &cdata) override {
    BuildChildren(field_map_);
    auto start = this->fbs_builder_->StartTable();
    ComposeChildren(field_map_);
    const auto end = this->fbs_builder_->EndTable(start);
    return SetOffset(end);
  }

  ItemType SetOffset(flatbuffers::uoffset_t o) {
    offset_ = o;
    return offset_;
  }

  FieldConfigMap field_map_;
  flatbuffers::uoffset_t offset_;
};

class FbsDeepHandler : public FbsHandler, public FbsGroupHelper {
 public:
  FbsDeepHandler(flatbuffers::FlatBufferBuilder *builder)
      : FbsHandler(builder, 0, false) {}
  ~FbsDeepHandler() {}

  virtual bool Open(XmlParser *parser, const XML_Char *name,
                    const XML_Char **atts) override {
    return FbsGroupHelper::Open(parser, name, atts, field_map_);
  }

  virtual bool Close(XmlParser *parser, const XML_Char *name,
                     const std::string &cdata) override {
    bool pop_on_close = false;
    if (!match_name_.compare(name)) {
      trace("matched end of element " << name);
      pop_on_close = true;
    } else {
      trace("reached end of unprocessed element " << name);
    }
    return pop_on_close;
  }

  virtual void Build() override { BuildChildren(field_map_); }

  virtual void Compose() override { ComposeChildren(field_map_); }

 protected:
  FieldConfigMap field_map_;
};

template <typename H, typename C>
class FbsUnionHandler : public FbsHandler {
 public:
  static_assert(std::is_enum<C>::value, "typename C must be an enum");
  static_assert(
      std::is_base_of<FbsMemberHandler<typename H::FbsType>, H>::value,
      "H shuld be derived from FbsMemberHandler<typename H::FbsType>");

  using union_type_t = std::underlying_type_t<C>;

  FbsUnionHandler(flatbuffers::FlatBufferBuilder *builder,
                  std::shared_ptr<H> handler_helper,
                  flatbuffers::voffset_t vt_field_type,
                  flatbuffers::voffset_t vt_field, C union_type)
      : FbsHandler(builder, vt_field, handler_helper->GetCollectCData()),
        handler_helper_(handler_helper),
        union_type_(union_type),
        vt_field_type_(vt_field_type),
        field_offset_(0) {}
  ~FbsUnionHandler() {}

  virtual void Build() override { BuildUnion(); }

  virtual void Compose() override {
    // fbb_.AddElement<uint8_t>(Order::VT_FIELD_CHOICE_TYPE,
    // static_cast<uint8_t>(field_choice_type), 0);
    if (field_offset_.o) {
      fbs_builder_->AddElement<union_type_t>(
          vt_field_type_, static_cast<union_type_t>(union_type_), 0);
      // fbb_.AddOffset(Order::VT_FIELD_CHOICE, field_choice);
      fbs_builder_->AddOffset(handler_helper_->GetVtField(), field_offset_);
    }
  }

  virtual bool Enter(XmlParser *parser, const XML_Char *name,
                     const XML_Char **atts) override {
    return handler_helper_->Enter(parser, name, atts);
  }

  virtual bool Close(XmlParser *parser, const XML_Char *name,
                     const std::string &cdata) override {
    return handler_helper_->Close(parser, name, cdata);
  }

  template <typename U = H,
            typename std::enable_if<
                std::is_base_of<flatbuffers::Table, typename U::FbsType>::value,
                int>::type = 0>
  void BuildUnion() {
    handler_helper_->Build();
    auto elements = handler_helper_->GetElements();
    if (elements.size()) {
      field_offset_ = elements[0].Union();
    }
  }

  template <
      typename U = H,
      typename std::enable_if<
          !std::is_base_of<flatbuffers::Table, typename U::FbsType>::value,
          int>::type = 0>
  void BuildUnion() {
    auto elements = handler_helper_->GetElements();
    if (elements.size()) {
      field_offset_ = fbs_builder_->CreateStruct(&elements[0]).Union();
    }
  }

  /*
   protected:
    template <typename U = H,
              typename std::enable_if<
                  std::is_base_of<flatbuffers::Table, typename
   U::FbsType>::value, int>::type = 0> void ComposeUnion() {}

    template <
        typename U = H,
        typename std::enable_if<
            !std::is_base_of<flatbuffers::Table, typename U::FbsType>::value,
            int>::type = 0>
    void ComposeUnion() {
      // fbb_.AddElement<uint8_t>(Order::VT_FIELD_CHOICE_TYPE,
   static_cast<uint8_t>(field_choice_type), 0);
      fbs_builder_->AddElement<union_type_t>(
          vt_field_type_, static_cast<union_type_t>(union_type_), 0);
      // fbb_.AddOffset(Order::VT_FIELD_CHOICE, field_choice);
      fbs_builder_->AddOffset(handler_helper_->GetVtField(), field_offset_);
    }
    */

  std::shared_ptr<H> handler_helper_;
  C union_type_;
  flatbuffers::voffset_t vt_field_type_;
  flatbuffers::Offset<void> field_offset_;
};

template <class THandler>
class FbsHandlerRoot : public XmlElementHandler {
 public:
  FbsHandlerRoot(const std::string &root_name)
      : fbs_builder_(1024), root_name_(root_name), handler_(&fbs_builder_) {}

  virtual ~FbsHandlerRoot() {}

  virtual bool Open(XmlParser *parser, const XML_Char *name,
                    const XML_Char **atts) override {
    if (!strcmp(name, root_name_.c_str())) {
      // change the state to <name>
      parser->PushElementHandler(&handler_);
      // notify the root handler about the attributes found in <name>
      return handler_.Enter(parser, name, atts);
    }
    return false;
  }

  virtual bool Close(XmlParser *parser, const XML_Char *name,
                     const std::string &data) override {
    return false;
  }

  virtual void Activate(XmlParser *parser, const XML_Char *name) override {
    // at this point we have reached </name>
    trace("activate from " << name);
    flatbuffers::uoffset_t o_root = handler_.GetOffset();
    flatbuffers::Offset<typename THandler::FbsType> root(o_root);
    fbs_builder_.Finish(root);
  }

  const flatbuffers::FlatBufferBuilder *GetFlatBufferBuilder() const {
    return &fbs_builder_;
  }

 private:
  flatbuffers::FlatBufferBuilder fbs_builder_;
  std::string root_name_;
  THandler handler_;
};

template <class THandler>
class XmlToFbsParser : public XmlParser {
 public:
  XmlToFbsParser(const std::string &root_name)
      : XmlParser(), handler_root_(root_name) {
    PushElementHandler(&handler_root_);
  }
  const flatbuffers::FlatBufferBuilder *GetFlatBufferBuilder() const {
    return handler_root_.GetFlatBufferBuilder();
  }

 private:
  FbsHandlerRoot<THandler> handler_root_;
};
}  // namespace flx

#endif
