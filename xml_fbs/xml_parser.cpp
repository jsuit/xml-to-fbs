#include "xml_parser.h"

namespace flx {
void XmlParser::StartElementCb(void *user_data, const XML_Char *name,
                               const XML_Char **attributes) {
  XmlParser *parser = static_cast<XmlParser *>(user_data);
  XmlElementHandler *handler = parser->GetCurrentHandler();
  if(handler) {
    parser->SetCollectData(handler->Open(parser, name, attributes));
  }
}

  void XmlParser::EndElementCb(void *user_data, const XML_Char *name) {
    XmlParser *parser = static_cast<XmlParser *>(user_data);
    XmlElementHandler *handler = parser->GetCurrentHandler();
    if(handler) {
      bool pop_after_close = handler->Close(parser, name, parser->cdata_);
      if(pop_after_close) {
        parser->PopElementHandler();
        handler = parser->GetCurrentHandler();
        if(handler) {
          handler->Activate(parser, name);
        }
      }
    }
    parser->CollectDataOff();
  }

  void XmlParser::CharacterDataCb(void *user_data, const XML_Char *buffer,
                                  int len) {
    XmlParser *parser = static_cast<XmlParser *>(user_data);
    if(parser->GetCollectData()) {
      parser->cdata_.append(buffer, len);
    }
  }
}  // namespace flx